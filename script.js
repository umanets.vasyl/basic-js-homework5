"use strict";

function createNewUser() {
  let newUser = {
    firstName: prompt("Enter your first name", "Denys"),
    lastName: prompt("Enter your last name", "Kravtsov"),

    getLogin() {
      return (
        this.firstName.toLowerCase().slice(0, 1) + this.lastName.toLowerCase()
      );
    },

    getFullName() {
      return `${this.firstName} ${this.lastName}`;
    },

    setFirstName(newFirstName) {
      Object.defineProperty(this, "firstName", {
        value: newFirstName,
        writable: true,
      });
    },

    setLastName(newLastName) {
      Object.defineProperty(this, "lastName", {
        value: newLastName,
        writable: true,
      });
    },
  };

  Object.defineProperty(newUser, "firstName", {
    configurable: true,
    writable: false,
  });

  Object.defineProperty(newUser, "lastName", {
    configurable: true,
    writable: false,
  });

  return newUser;
}

let userOne = new createNewUser();

console.log(`User full name is ${userOne.getFullName()}`);
console.log(`User login is ${userOne.getLogin()}`);

userOne.setFirstName("Viktor");
userOne.setLastName("Mykolenko");

console.log(`User full name is ${userOne.getFullName()}`);
console.log(`User login is ${userOne.getLogin()}`);
